redis samples
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Detail](#2-samples)
  1. [single](#21-sinfgle)
  2. [TODO](#22-todo)


# 1. Introduction
samples for redis

## 2.1. single

```bash
docker-compose up -d --build
```

enter redis-cli
```bash
docker exec -it redis redis-cli
```
manipulation: several samples:
INFO about instance
```bash
docker exec -it redis redis-cli INFO
```
set key/value
```bash
docker exec -it redis redis-cli SET key1 value1
```
get key
```bash
docker exec -it redis redis-cli GET key1
```
etc...


## 2.2. TODO

